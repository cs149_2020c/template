package check;

public class Spinner {
  public static void main(String[] args) throws InterruptedException {
    String s = "|/-\\";
    int i = 0;
    while (true) {
      Thread.sleep(100);
      System.out.print("\r" + s.charAt(i));
      i = (i + 1) % s.length();
    }
  }
}